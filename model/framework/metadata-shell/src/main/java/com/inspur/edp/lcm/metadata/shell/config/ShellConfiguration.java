/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.shell.config;

import com.inspur.edp.lcm.metadata.shell.StringCommandRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Classname RefConfiguration Description ref配置类 Date 2019/11/28 10:34
 *
 * @author zhongchq
 * @version 1.0
 */
@Configuration
public class ShellConfiguration {
    @Bean
    public StringCommandRunner create() {
        return new StringCommandRunner();
    }
}
