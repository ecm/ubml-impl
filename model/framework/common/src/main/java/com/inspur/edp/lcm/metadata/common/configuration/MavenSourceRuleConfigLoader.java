/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import io.iec.edp.caf.common.environment.EnvironmentUtil;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

/**
 * @Classname MavenSourceRuleConfigLoader
 * @Description TODO
 * @Date 2019/12/2 19:13
 * @Created by liu_bintr
 * @Version 1.0
 */
public class MavenSourceRuleConfigLoader {
    private static String fileName = "config/platform/common/lcm_mavensourcerule.json";
    private static String sectionName = "rule";

    public static Map<String, List<String>> mavenSourceRuleConfigurations;

    public MavenSourceRuleConfigLoader() {
        fileName = Paths.get(EnvironmentUtil.getServerRTPath()).resolve(fileName).toString();
    }

    public static Map<String, List<String>> getMavenSourceRuleConfigurations() {
        return mavenSourceRuleConfigurations;
    }

    public static void setMavenSourceRuleConfigurations(
        Map<String, List<String>> mavenSourceRuleConfigurations) {
        MavenSourceRuleConfigLoader.mavenSourceRuleConfigurations = mavenSourceRuleConfigurations;
    }

    protected Map<String, List<String>> loadSourceRuleConfigration() {
        MetadataServiceHelper metadataServiceHelper = new MetadataServiceHelper();
        try {
            if (mavenSourceRuleConfigurations == null || mavenSourceRuleConfigurations.size() <= 0) {
                mavenSourceRuleConfigurations = metadataServiceHelper.getMavenSourceRule(fileName, sectionName);
                return mavenSourceRuleConfigurations;
            } else {
                return mavenSourceRuleConfigurations;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
