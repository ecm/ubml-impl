/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.inspur.edp.lcm.workspace.api.WorkSpaceService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

/**
 * @author zhaoleitr
 */
public class WorkspaceHelper {
    private WorkSpaceService wsService;
    private static WorkspaceHelper singleton;

    public WorkspaceHelper() {
        wsService = SpringBeanUtils.getBean(WorkSpaceService.class);
    }

    public static WorkspaceHelper getInstance() {
        if (singleton == null) {
            singleton = new WorkspaceHelper();
        }
        return singleton;
    }

    public String getDevRootPath() {
//        return "F:\\projects";
        return wsService.getUserBasePath().replace("\\", "/");
        /*
        MetadataServiceHelper metadataServiceHelper = new MetadataServiceHelper();
        try {

                final String combinePath = FileUtils.getCombinePath(caf_boot_home, workspacePath);
                    return metadataServiceHelper.getDevRootPath(combinePath);
            return metadataServiceHelper.getDevRootPath(workspacePath);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        */
    }
}
