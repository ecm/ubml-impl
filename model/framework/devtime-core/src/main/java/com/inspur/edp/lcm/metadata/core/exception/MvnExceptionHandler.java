/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.exception;

import io.iec.edp.caf.commons.exception.handle.DefaultExceptionHandler;

public class MvnExceptionHandler extends DefaultExceptionHandler {
    private static final String ERROR_KEYWORD = "[ERROR]";
    private String messageHandled = "";

    public String handleMessage(String message) {
        handleNotFindSymbolError(message);
        if (!messageHandled.isEmpty()) {
            return messageHandled;
        }

        handleError(message, MvnExceptionKeyword.NOT_EXIST, MvnExceptionSolution.NOT_EXIST_SOLUTION);
        if (!messageHandled.isEmpty()) {
            return messageHandled;
        }

        handleError(message, MvnExceptionKeyword.NOT_ABSTRACT, MvnExceptionSolution.NOT_ABSTRACT_SOLUTION);
        if (!messageHandled.isEmpty()) {
            return messageHandled;
        }

        handleError(message, MvnExceptionKeyword.DEFINED_NAME, MvnExceptionSolution.DEFINED_NAME_SOLUTION);
        if (!messageHandled.isEmpty()) {
            return messageHandled;
        }

        handleError(message, MvnExceptionKeyword.NOT_BE_RESOLVED, MvnExceptionSolution.NOT_BE_RESOLVED_SOLUTION);
        if (!messageHandled.isEmpty()) {
            return messageHandled;
        }

        handleClassNotAvailableError(message);
        if (!messageHandled.isEmpty()) {
            return messageHandled;
        }

        handleNotAccessableError(message);
        if (!messageHandled.isEmpty()) {
            return messageHandled;
        }

        handleError(message, MvnExceptionKeyword.ERROR_IN_OPENING_ZIP_FILE, MvnExceptionSolution.ERROR_IN_OPENING_ZIP_FILE_SOLUTION);
        if (!messageHandled.isEmpty()) {
            return messageHandled;
        }

        return messageHandled;
    }

    private void handleNotFindSymbolError(String message) {
        if (message.contains(MvnExceptionKeyword.NOT_FIND_SYMBOL)) {
            final String notFindSymbolString = findErrorStringByKeyword(message, MvnExceptionKeyword.NOT_FIND_SYMBOL);
            final String symbolString = findErrorStringByKeyword(message, MvnExceptionKeyword.SYMBOL);
            final String symbolLocationString = findErrorStringByKeyword(message, MvnExceptionKeyword.SYMBOL_LOCATION);
            messageHandled += MvnExceptionSolution.NOT_FIND_SYMBOL_SOLUTION + notFindSymbolString + symbolString + symbolLocationString + MvnExceptionSolution.LOG_REMINDER;
        }
    }

    private void handleClassNotAvailableError(String message) {
        if (message.contains(MvnExceptionKeyword.CLASS_NOT_AVAILABLE)) {
            final String classNotAvailableString = findErrorStringByKeyword(message, MvnExceptionKeyword.CLASS_NOT_AVAILABLE);
            final String needString = findErrorStringByKeyword(message, MvnExceptionKeyword.NEED);
            final String findString = findErrorStringByKeyword(message, MvnExceptionKeyword.FIND);
            final String reasonString = findErrorStringByKeyword(message, MvnExceptionKeyword.REASON);
            messageHandled += MvnExceptionSolution.CLASS_NOT_AVAILABLE_SOLUTION + classNotAvailableString + needString + findString + reasonString + MvnExceptionSolution.LOG_REMINDER;
        }
    }

    private void handleNotAccessableError(String message) {
        if (message.contains(MvnExceptionKeyword.NOT_ACCESSIBLE)) {
            final String classNotAccessableString = findErrorStringByKeyword(message, MvnExceptionKeyword.NOT_ACCESSIBLE);
            final String classNotFindString = findErrorStringByKeyword(message, MvnExceptionKeyword.NOT_FIND);
            messageHandled += MvnExceptionSolution.NOT_ACCESSIBLE_SOLUTION + classNotAccessableString + classNotFindString + MvnExceptionSolution.LOG_REMINDER;
        }
    }

    private void handleError(String message, String keyword, String solution) {
        if (message.contains(keyword)) {
            final String errorStringByKeyword = findErrorStringByKeyword(message, keyword);
            messageHandled += solution + errorStringByKeyword + MvnExceptionSolution.LOG_REMINDER;
        }
    }

    private String findErrorStringByKeyword(String message, String keyword) {
        String errorString = "";
        final int keywordIndex = message.indexOf(keyword);
        if (keywordIndex > -1) {
            int lastErrorIndex = message.substring(0, keywordIndex).lastIndexOf(ERROR_KEYWORD);
            lastErrorIndex = lastErrorIndex == -1 ? ERROR_KEYWORD.length() : lastErrorIndex + ERROR_KEYWORD.length();

            int nextErrorIndex = message.substring(keywordIndex).indexOf(ERROR_KEYWORD) + keywordIndex;
            if (nextErrorIndex == -1) {
                errorString = message.substring(lastErrorIndex).trim();
            } else {
                errorString = message.substring(lastErrorIndex, nextErrorIndex).trim();
            }
        }
        return errorString + "。";
    }
}
